#!/bin/bash -ex

LINK="http://dl.google.com/earth/client/advanced/current/GoogleEarthProMac-Intel.dmg"

URL=`curl -I "${LINK}" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36' 2>/dev/null | grep '^Etag' | tail -1 | sed 's/Etag: //' | tr -d '\r'`

if [ "x${URL}" != "x" ]; then
    echo URL: "${URL}"
    echo "${URL}" > current-url
fi

# Update to handle distributed builds
if cmp current-url old-url; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-url old-url
    exit 0
fi