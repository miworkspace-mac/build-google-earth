#!/bin/bash -ex

# CONFIG
prefix="GoogleEarthPro"
suffix=""
munki_package_name="GoogleEarthPro"
display_name="Google Earth Pro"
icon_name=""
description="Google Earth Pro gives you a wealth of imagery and geographic information. Explore destinations like Maui and Paris, or browse content from Wikipedia, National Geographic, and more."
url="http://dl.google.com/earth/client/advanced/current/GoogleEarthProMac-Intel.dmg"

# download it (-L: follow redirects)
curl -L -o appdl.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse appdl.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
file=`ls ${mountpoint}/*.pkg`
/usr/sbin/pkgutil --expand-full "${file}" pkgpro

mkdir -p build-rootpro/Applications
drive_pkg=$(ls -d pkgpro/G*_Pro.pkg)

cp -R "$drive_pkg"/Payload/*.app build-rootpro/Applications


#(cd pkgpro; pax -rz -f *Pro.pkg/Payload; mv *Pro.app ../build-rootpro/Applications )

hdiutil detach "${mountpoint}"

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" build-rootpro/Applications/*.app/Contents/Info.plist`

## Create pkg's
/usr/bin/pkgbuild --root build-rootpro/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version apppro.pkg

#hdiutil create -srcfolder build-rootpro -format UDZO -o apppro.dmg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-rootpro -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo apppro.pkg "${key_files}" | /bin/bash > apppro.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-rootpro//' apppro.plist
## END EXAMPLE

# Build pkginfo
# /usr/local/munki/makepkginfo app.dmg > app.plist

plist=`pwd`/apppro.plist
pwd=`pwd`
# Obtain version info
#cp -R "${pwd}"/build-root/Google\ Earth.app/Contents/Info.plist build-root
#version=`defaults read "${pwd}"/build-rootpro/Google\ Earth\ Pro.app/Contents/Info.plist CFBundleVersion`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.8.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain update description from MacUpdate and add to plist
# description=`/usr/local/bin/mutool --update 11942` #(update to corresponding MacUpdate number)
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Set version comparison key
/usr/libexec/PlistBuddy -c "Set :installs:0:version_comparison_key CFBundleVersion" apppro.plist


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv apppro.pkg   ${prefix}-${version}${suffix}.pkg
mv apppro.plist ${prefix}-${version}${suffix}.plist
