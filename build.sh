#!/bin/bash -ex

# CONFIG
prefix="GoogleEarth"
suffix=""
munki_package_name="GoogleEarth"
display_name="Google Earth"
icon_name=""
description="Google Earth gives you a wealth of imagery and geographic information. Explore destinations like Maui and Paris, or browse content from Wikipedia, National Geographic, and more."
url="http://dl.google.com/earth/client/advanced/current/GoogleEarthMacNoUpdate-Intel.dmg"

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
file=`ls ${mountpoint}/*.pkg`
/usr/sbin/pkgutil --expand "${file}" temp

#mkdir build-root   ; pax -rz -f usb*.pkg/Payload 
(cd temp; pax -rz -f *rth.pkg/Payload)

mkdir -p build-root/Applications
cp -R temp/*.app build-root/Applications

hdiutil detach "${mountpoint}"

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" build-root/Applications/*.app/Contents/Info.plist`

## Create pkg's
#/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version app.pkg


#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

#clean up the component file
rm -rf Component-${munki_package_name}.plist


## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo app.pkg "${key_files}" | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

## END EXAMPLE
 
# Build pkginfo
# /usr/local/munki/makepkginfo app.dmg > app.plist

plist=`pwd`/app.plist
pwd=`pwd`
# Obtain version info
#cp -R "${pwd}"/build-root/Google\ Earth.app/Contents/Info.plist build-root
#version=`defaults read "${pwd}"/build-root/Google\ Earth.app/Contents/Info.plist CFBundleVersion`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.8.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"




# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
